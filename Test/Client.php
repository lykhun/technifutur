<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 13-09-18
 * Time: 09:32
 */

namespace Test;


class Client
{
    private $name;
    private $firstName;
    private $accountNumber;

    /**
     * @var BaseCompte[]
     */
    private $comptes;

    public function getComptes(){
        return $this->comptes;
    }

    public function addCompte(BaseCompte $c)
    {
        $this->comptes[] = $c;
        $c->setClient($this);
    }

    public function removeCompte(BaseCompte $c)
    {
        foreach ($this->comptes as $key => $compte){
            if ($c == $compte){
                unset($this->comptes[$key]);
            }
        }
    }

    public function __construct($accountN)
    {
        if (strlen($accountN) != 20)
        {
            throw new \InvalidArgumentException();
        }
        $this->accountNumber = $accountN;
        $this->comptes = [];
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @throws \Exception
     */
    public function setName($name)
    {
        if ($name == null){
            throw new \Exception();
        }
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }




}