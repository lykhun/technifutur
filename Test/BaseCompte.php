<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 13-09-18
 * Time: 10:04
 */

namespace Test;


abstract class BaseCompte
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $number;

    /**
     * @var float
     */
    protected $solde;

    public function __construct($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     * @return BaseCompte
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
        //$client->addCompte($this);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     * @return BaseCompte
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSolde()
    {
        return $this->solde;
    }

    public function retirer($montant)
    {
        if ($montant <= 0){
            throw new \InvalidArgumentException();
        }
    }

    public function ajouter($montant){
        if ($montant <= 0){
            throw new \InvalidArgumentException();
        }
        $this->solde = $this->getSolde() + $montant;
    }

}