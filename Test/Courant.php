<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 13-09-18
 * Time: 10:02
 */

namespace Test;


class Courant extends BaseCompte
{
    /**
     * @var float
     */
    private $limiteDeCredit;

    /**
     * @return float
     */
    public function getLimiteDeCredit()
    {
        return $this->limiteDeCredit;
    }

    /**
     * @param float $limiteDeCredit
     * @return Courant
     */
    public function setLimiteDeCredit($limiteDeCredit)
    {
        if ($limiteDeCredit > 10000){
            throw new \InvalidArgumentException();
        }
        $this->limiteDeCredit = $limiteDeCredit;
        return $this;
    }

    public function retirer($montant)
    {
        parent::retirer($montant);
        if ($montant > $this->getSolde() + $this->limiteDeCredit){
            throw new \InvalidArgumentException();
        }
        $this->solde -= $montant;
    }


}