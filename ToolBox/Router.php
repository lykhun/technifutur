<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 19-09-18
 * Time: 14:42
 */

namespace ToolBox;


class Router
{
    public function handleRoute($twig, $get, $post)
    {
        if (isset($get["controller"])){
            $controllerName = $get["controller"];
            $className = "\\PhpOO\\Controllers\\"
                . ucwords($controllerName)
                . "Controller";
            $controller = new $className($twig);
            if (isset($get["method"])){
                if (isset($get["id"])){
                    if (empty($post)){
                        return $controller->{$get["method"]}($get["id"]);
                    }
                    else{
                        return $controller->{$get["method"]}($get["id"], $post);
                    }
                }
                else{
                    if (empty($post)){
                        return $controller->{$get["method"]}();
                    }
                    else{
                        return $controller->{$get["method"]}($post);
                    }

                }
            }
        }
    }
}