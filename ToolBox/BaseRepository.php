<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 12-09-18
 * Time: 13:11
 */

namespace ToolBox;


use PDO;

abstract class BaseRepository
{
    protected $pdo;
    public function __construct()
    {
        $this->pdo = new PDO(
            "mysql:host=localhost;dbname=phpoo",
            "root",
            null
        );
    }
    protected abstract function getTableName();
    protected abstract function getPKName();
    protected abstract function getEntityName();
    protected abstract function getBindings();


    public function getByID($id)
    {
        $query = "SELECT ";
        foreach ($this->getBindings() as $key => $value){
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= " ,";

        }
        $query .= $this->getPKName();

        $query .= " FROM ";
        $query .= $this->getTableName();
        $query .= " WHERE ";
        $query .= $this->getPKName();
        $query .= " = :id";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute([":id" => $id]);
        return $stmt->fetchObject($this->getEntityName());
    }

    public function getAll()
    {
        $query = "SELECT ";
        foreach ($this->getBindings() as $key => $value){
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= " ,";
        }
        $query .= $this->getPKName();

        $query .= " FROM ";
        $query .= $this->getTableName();
        $stmt = $this->pdo->prepare($query);
        $list = [];
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS, $this->getEntityName());
    }

    public function insert($item)
    {
        $bindings = $this->getBindings();
        $query = "INSERT INTO ";
        $query .= $this->getTableName();
        $query .= "(";
        $query .= implode(",", array_keys($bindings));
        $query .= ") VALUES (:";
        $query .= implode(",:", array_keys($bindings));
        $query .= ")";
        $stmt = $this->pdo->prepare($query);
        $tab = [];
        foreach ($bindings as $key => $value){
            $tab[":" . $key] = $item->{"get".ucwords($value)}();
        }
        $result = $stmt->execute($tab);
        if ($result){
            $item->{"set" . ucwords($this->getPKName())}
            ($this->pdo->lastInsertId());
        }
        return $result;
    }

    public function Update($item){
        $bindings = $this->getBindings();
        $query = "UPDATE ";
        $query .= $this->getTableName();
        $query .= " set ";
        foreach ($bindings as $key => $value){
            $query .= $key;
            $query .= "=:";
            $query .= $key;
            $query .= ",";
        }
        $query = substr($query, 0, -1);
        $query .= " WHERE ";
        $query .= $this->getPKName();
        $query .= " = :";
        $query .= $this->getPKName();
        $stmt = $this->pdo->prepare($query);
        $tab = [];
        foreach ($bindings as $key => $value){
            $tab[":" . $key] = $item->{"get".ucwords($value)}();
        }
        $tab[":" . $this->getPKName()] = $item->{$this->getPKName()};
        $result = $stmt->execute($tab);
        return $result;
    }


    public function delete($id)
    {
        $query = "DELETE FROM ";
        $query .= $this->getTableName();
        $query .= " WHERE ";
        $query .= $this->getPKName();
        $query .= " = :id";
        $stmt = $this->pdo->prepare($query);

        $result = $stmt->execute([":id" => $id]);
        return $result;

    }
}