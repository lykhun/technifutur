<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 11-09-18
 * Time: 13:16
 */

namespace PhpOO\Models;


use PhpOO\Repository\SupplierRepository;

class Product_Supplier
{
    private $product_supplierID;
    private $productID;
    private $supplierID;
    private $price;

    private $supplier;

    public function getSupplier(){
        if ($this->supplier == null){
            $repo = new SupplierRepository();
            $this->supplier = $repo->getByID($this->supplierID);
        }
        return $this->supplier;
    }

    /**
     * @return mixed
     */
    public function getProduct_supplierID()
    {
        return $this->product_supplierID;
    }

    /**
     * @param mixed $product_supplierID
     * @return Product_Supplier
     */
    public function setProduct_supplierID($product_supplierID)
    {
        $this->product_supplierID = $product_supplierID;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getProductID()
    {
        return $this->productID;
    }

    /**
     * @param mixed $productID
     * @return Product_Supplier
     */
    public function setProductID($productID)
    {
        $this->productID = $productID;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSupplierID()
    {
        return $this->supplierID;
    }

    /**
     * @param mixed $supplierID
     * @return Product_Supplier
     */
    public function setSupplierID($supplierID)
    {
        $this->supplierID = $supplierID;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Product_Supplier
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }


}