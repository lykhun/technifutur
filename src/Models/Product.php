<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 11-09-18
 * Time: 13:11
 */

namespace PhpOO\Models;


use PhpOO\Repository\ProductSupplierRepository;
use PhpOO\Repository\SupplierRepository;

class Product
{
    private $productID;

    private $productName;

    private $productDescription;

    private $productImage;

    private $productSuppliers;

    public function getProductSuppliers()
    {
        if ($this->productSuppliers == null ){
            //initialisation de productSuppliers
            $repo = new ProductSupplierRepository();
            $this->productSuppliers
                = $repo->getByProductID($this->productID);

        }
        return $this->productSuppliers;
    }

    /**
     * @return mixed
     */
    public function getProductID()
    {
        return $this->productID;
    }

    /**
     * @param mixed $productID
     */
    public function setProductID($productID)
    {
        $this->productID = $productID;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param mixed $productName
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
    }

    /**
     * @return mixed
     */
    public function getProductDescription()
    {
        return $this->productDescription;
    }

    /**
     * @param mixed $productDescription
     */
    public function setProductDescription($productDescription)
    {
        $this->productDescription = $productDescription;
    }

    /**
     * @return mixed
     */
    public function getProductImage()
    {
        return $this->productImage;
    }

    /**
     * @param mixed $productImage
     */
    public function setProductImage($productImage)
    {
        $this->productImage = $productImage;
    }



}