<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 11-09-18
 * Time: 13:14
 */

namespace PhpOO\Models;


class Supplier
{
    private $supplierID;
    private $supplierName;
    private $supplierAddress;
    private $supplierEmail;

    /**
     * @return mixed
     */
    public function getSupplierID()
    {
        return $this->supplierID;
    }

    /**
     * @param mixed $supplierID
     * @return Supplier
     */
    public function setSupplierID($supplierID)
    {
        $this->supplierID = $supplierID;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSupplierName()
    {
        return $this->supplierName;
    }

    /**
     * @param mixed $supplierName
     * @return Supplier
     */
    public function setSupplierName($supplierName)
    {
        $this->supplierName = $supplierName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSupplierAddress()
    {
        return $this->supplierAddress;
    }

    /**
     * @param mixed $supplierAddress
     * @return Supplier
     */
    public function setSupplierAddress($supplierAddress)
    {
        $this->supplierAddress = $supplierAddress;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSupplierEmail()
    {
        return $this->supplierEmail;
    }

    /**
     * @param mixed $supplierEmail
     * @return Supplier
     */
    public function setSupplierEmail($supplierEmail)
    {
        $this->supplierEmail = $supplierEmail;
        return $this;
    }



}