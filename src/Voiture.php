<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 11-09-18
 * Time: 09:29
 */
namespace PhpOO;
class Voiture
{
    const BruitDuKlaxon = "Pouet Pouet";
    public $couleur = "Bleu";

    public $marque;

    public function Klaxonner()
    {
        echo self::BruitDuKlaxon;
    }

    public function SeDecrire()
    {
        echo "Je suis de couleur " . $this->couleur;
    }
}