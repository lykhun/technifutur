<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 20-09-18
 * Time: 13:49
 */

namespace PhpOO\Repository;


use PDO;
use PhpOO\Models\Product_Supplier;
use ToolBox\BaseRepository;

class ProductSupplierRepository extends BaseRepository
{

    public function getByProductID($productID)
    {
        $stmt = $this->pdo->prepare(
            "SELECT * FROM product_supplier WHERE productID = :id"
        );
        $stmt->execute([":id" => $productID]);
        return $stmt->fetchAll(PDO::FETCH_CLASS,
            Product_Supplier::class);
    }

    protected function getTableName()
    {
        return "product_supplier";
    }

    protected function getPKName()
    {
        return "product_supplierID";
    }

    protected function getEntityName()
    {
        return Product_Supplier::class;
    }

    protected function getBindings()
    {
        return [
            "productID" => "productID",
            "supplierID" => "supplierID",
            "price" => "price"
        ];
    }
}