<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 12-09-18
 * Time: 14:23
 */

namespace PhpOO\Repository;


use PhpOO\Models\Supplier;
use ToolBox\BaseRepository;

class SupplierRepository extends BaseRepository
{

    protected function getTableName()
    {
        return "supplier";
    }

    protected function getPKName()
    {
        return "supplierID";
    }

    protected function getEntityName()
    {
        return Supplier::class;
    }

    protected function getBindings()
    {
        return [
            "supplierName" => "supplierName",
            "supplierEmail" => "supplierEmail",
            "supplierAddress" => "supplierAddress"
        ];
    }
}