<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 11-09-18
 * Time: 15:42
 */

namespace PhpOO\Repository;

use PhpOO\Models\Product;
use ToolBox\BaseRepository;


class ProductRepository extends BaseRepository
{
    protected function getTableName()
    {
        return "product";
    }

    protected function getPKName()
    {
        return "productID";
    }

    protected function getEntityName()
    {
        return Product::class;
    }

    protected function getBindings()
    {
        return [
            //nom des cols => nom des variables
            "productName" => "productName",
            "productDescription" => "productDescription",
            "productImage" => "productImage"
        ];
    }
}