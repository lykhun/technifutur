<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 20-09-18
 * Time: 08:43
 */

namespace PhpOO\Controllers;


use PhpOO\Models\Supplier;
use PhpOO\Repository\SupplierRepository;

class SupplierController
{
    private $twig;
    public function __construct($twig)
    {
        $this->twig = $twig;
    }
    public function index()
    {
         $repo = new SupplierRepository();
         $suppliers = $repo->getAll();
         return $this->twig->render("allSuppliers.html.twig", [
             "suppliers" => $suppliers
         ]);
    }

    public function add($post = null)
    {
        if (isset($post["submit"])){
            //insert
            $valid = true;
            //ajouter les validations
            $supplier = new Supplier();
            //hydrater le model
            $supplier->setSupplierName($post["name"]);
            $supplier->setSupplierEmail($post["email"]);
            $supplier->setSupplierAddress($post["address"]);
            if ($valid){
                $repo = new SupplierRepository();
                $repo->insert($supplier);
                header("location:http://localhost/phpoo/web/app.php?controller=supplier&method=index");
            }
            else{
                return $this->twig->render("addSupplier.html.twig", [
                    "supplier" => $supplier
                ]);
            }
        }
        else{
            //afficher la vue
            return $this->twig->render("addSupplier.html.twig");
        }
    }
}