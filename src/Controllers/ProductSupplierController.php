<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 20-09-18
 * Time: 13:26
 */

namespace PhpOO\Controllers;


use PhpOO\Models\Product_Supplier;
use PhpOO\Repository\ProductRepository;
use PhpOO\Repository\ProductSupplierRepository;
use PhpOO\Repository\SupplierRepository;

class ProductSupplierController
{
    private $twig;

    public function __construct($twig)
    {
        $this->twig = $twig;
    }

    public function add($post = null)
    {
        $repoP = new ProductRepository();
        $repoS = new SupplierRepository();
        $products = $repoP->getAll();
        $suppliers = $repoS->getAll();
        if (isset($post["submit"])){
            $isValid = true;
            $ps = new Product_Supplier();
            $ps->setProductID($post["productID"])
                ->setSupplierID($post["supplierID"])
                ->setPrice($post["price"]);
            if ($isValid){
                $repo = new ProductSupplierRepository();
                $repo->insert($ps);
                header("location:http://localhost/phpoo/web/app.php?controller=product&method=index");
            }
        }
        else{
            return $this->twig->render("product_supplier_add.html.twig",[
                "products" => $products,
                "suppliers" => $suppliers
            ]);
        }
    }
}