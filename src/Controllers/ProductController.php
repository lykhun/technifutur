<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 18-09-18
 * Time: 13:29
 */

namespace PhpOO\Controllers;


use PhpOO\Models\Product;
use PhpOO\Repository\ProductRepository;

class ProductController
{
    private $twig;
    public function __construct($twig)
    {
        $this->twig = $twig;
    }

    public function index()
    {
        $repo = new ProductRepository();
        $list = $repo->getAll();
        return $this->twig->render("productList.html.twig",
            ["listP" => $list]
        );
    }

    public function details($id)
    {
        $repo = new ProductRepository();
        $prod = $repo->getByID($id);
        return $this->twig->render("productDetails.html.twig", [
            "prod" => $prod
        ]);
    }

    public function newProd($post = null)
    {

        if(isset($post["submit"])){
            //hydratation
            $prod = new Product();
            $prod->setProductName($post["nom"]);
            $prod->setProductDescription($post["desc"]);
            $prod->setProductImage($post["image"]);
            //si le formulaire est valide
            $isValid = true;
            $errors = [];
            if (strlen(trim($post["nom"])) == 0){
                $isValid = false;
                $errors["productName"] = "Le champs est requis";
            }
            if (strlen(trim($post["desc"])) == 0){
                $isValid = false;
                $errors["productDescription"] = "Le champs est requis";
            }
            if (strlen(trim($post["image"])) == 0){
                $isValid = false;
                $errors["productImage"] = "Le champs est requis";
            }
            if ($isValid){
                $repo = new ProductRepository();
                if($repo->insert($prod)){
                    header("location:http://localhost/phpoo/web/app.php?controller=product&method=index");
                }
            }
            else{
                return $this->twig->render("productNew.html.twig", [
                    "prod" => $prod,
                    "errors" => $errors
                ]);
            }

        }
        else{
            return $this->twig->render("productNew.html.twig");
        }

    }
}