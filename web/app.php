<?php
use ToolBox\Router;
use Twig\TwigFunction;

require_once "../vendor/autoload.php";
$loader = new Twig_Loader_Filesystem('../src/Views');
$twig = new Twig_Environment($loader);
$twigF = new TwigFunction("path", function($controller, $method, $params = null){
    $path = $_SERVER["REQUEST_SCHEME"];
    $path .= "://";
    $path .= $_SERVER["SERVER_NAME"];
    $path .= $_SERVER["SCRIPT_NAME"];
    $path .= "/?controller=";
    $path .= $controller;
    $path .= "&method=";
    $path .= $method;
    if ($params != null){
        foreach ($params as $key => $value){
            $path .= "&";
            $path .= $key;
            $path .= "=";
            $path .= $value;
        }
    }
    return $path;
});
$twig->addFunction($twigF);

$router = new Router();
echo $router->handleRoute($twig, $_GET, $_POST);



